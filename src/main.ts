import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import ElementPlus from 'element-plus';
// import Layui from 'layui';
import 'element-plus/lib/theme-chalk/index.css';
import './assets/css/icon.css'
////引入layui框架
import './assets/layui/css/layui.css'
import './assets/layui/layui.js'
/////引入jquery
// import $ from 'jquery'

createApp(App).use(store).use(router).use(ElementPlus).mount('#app')



