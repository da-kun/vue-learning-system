import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router'
import Home from '../views/Home.vue'

const routes: Array<RouteRecordRaw> = [
  {
    path: '/',
    redirect: '/course'
  },
  {
    path: '/',
    name: 'Home',
    component: Home,
    children: [
      {
        path: "/course",
        name: "course",
        meta: {
          title: '暖心教程'
        },
        component: () => import(
          "../views/Course.vue")
      },
      {
        path: '/datastructure',
        name: 'datastructure',
        meta: {
          title: '数据结构'
        },
        component: () => import(
          "../views/basicCompulsory/datastructure.vue")
      },
      {
        path: '/computer',
        name: 'computer',
        meta: {
          title: '计算机操作系统'
        },
        component: () => import(
          "../views/basicCompulsory/computer.vue")
      },
      {
        path: '/network',
        name: 'network',
        meta: {
          title: '数据结构'
        },
        component: () => import(
          "../views/basicCompulsory/network.vue")
      },
      {
        path: '/java',
        name: 'java',
        meta: {
          title: 'java'
        },
        component: () => import(
          "../views/programming/java.vue")
      },
      {
        path: '/cplus',
        name: 'cplus',
        meta: {
          title: 'C'
        },
        component: () => import(
          "../views/programming/cplus.vue")
      },
      {
        path: '/c',
        name: 'c',
        meta: {
          title: 'C语言'
        },
        component: () => import(
          "../views/programming/c.vue")
      },
      {
        path: '/python',
        name: 'python',
        meta: {
          title: 'python'
        },
        component: () => import(
          "../views/programming/python.vue")
      },
      {
        path: '/php',
        name: 'php',
        meta: {
          title: 'php'
        },
        component: () => import(
          "../views/programming/php.vue")
      },
      {
        path: '/typescript',
        name: 'typescript',
        meta: {
          title: 'typescript'
        },
        component: () => import(
          "../views/programming/typescript.vue")
      },
      {
        path: '/javascript',
        name: 'javascript',
        meta: {
          title: 'javascript'
        },
        component: () => import(
          "../views/programming/javascript.vue")
      },
      {
        path: '/frontend',
        name: 'frontend',
        meta: {
          title: 'frontend'
        },
        component: () => import(
          "../views/industryField/frontend.vue")
      },
      {
        path: '/rearend',
        name: 'rearend',
        meta: {
          title: 'rearend'
        },
        component: () => import(
          "../views/industryField/rearend.vue")
      },
      {
        path: '/ai',
        name: 'ai',
        meta: {
          title: 'ai'
        },
        component: () => import(
          "../views/industryField/ai.vue")
      },
      {
        path: '/bigdata',
        name: 'bigdata',
        meta: {
          title: 'bigdata'
        },
        component: () => import(
          "../views/industryField/bigdata.vue")
      },
      {
        path: '/applet',
        name: 'applet',
        meta: {
          title: 'applet'
        },
        component: () => import(
          "../views/industryField/applet.vue")
      },
      {
        path: '/linux',
        name: 'linux',
        meta: {
          title: 'linux'
        },
        component: () => import(
          "../views/industryField/linux.vue")
      },
      {
        path: '/andriod',
        name: 'andriod',
        meta: {
          title: 'andriod'
        },
        component: () => import(
          "../views/industryField/andriod.vue")
      },
      {
        path: '/information',
        name: 'information',
        meta: {
          title: 'information'
        },
        component: () => import(
          "../views/industryField/information.vue")
      },
      {
        path: '/job',
        name: 'job',
        meta: {
          title: 'job'
        },
        component: () => import(
          "../views/job.vue")
      },
      {
        path: '/programmer',
        name: 'programmer',
        meta: {
          title: 'programmer'
        },
        component: () => import(
          "../views/programmer.vue")
      },
      {
        path: '/welfare',
        name: 'welfare',
        meta: {
          title: 'welfare'
        },
        component: () => import(
          "../views/welfare.vue")
      },
    ]
  },
  // {
  //   path: '/about',
  //   name: 'About',
  //   component: () => import(/* webpackChunkName: "about" */ '../views/About.vue')
  // },
  {
    path: '/login',
    name: 'Login',
    meta: {
      title: '登录'
    },
    component: () => import('../views/Login.vue')
  }
]

const router = createRouter({
  history: createWebHashHistory(),
  routes
})
router.beforeEach((to, from, next) => {
  document.title = `vue-admin`;
  const role = localStorage.getItem('username');
  if (!role && to.path !== '/login') {
    next('/login');
  } else if (to.meta.permission) {
    // 如果是管理员权限则可进入，这里只是简单的模拟管理员权限而已
    role === 'admin'
      ? next()
      : next('/403');
  } else {
    next();
  }
});
export default router
